#!/usr/bin/env bash
#################################################
# Yazan		: Yahya YILDIRIM 
# E-Posta	: yahya.yldrm@gmail.com	
# GitHub	: https://github.com/yahyayildirim/
# Gitlab	: https://gitlab.com/yahyayildirim/
# AKKP		: https://kod.pardus.org.tr/yahyayildirim/
# Bu script kişisel kullanım için oluşturulmuştur..
#################################################

if [[ "$(whoami)" != root ]]; then
	echo -e "===>\tLütfen yetkili hesap ile işlem yapınız."
	echo -e "===>\tÖrnek: sudo bash $0"
	exit 1
fi

echo -e "===>\tFTP Protokolü için vsftpd paketi indirilecek ve kurulacaktır... Lütfen bekleyiniz."
if [ ! -z /var/lib/dpkg/lock ] || [ ! -z /var/lib/dpkg/lock-frontend ]; then
	rm -rf /var/lib/dpkg/lock
	rm -rf /var/lib/dpkg/lock-frontend
	rm -rf /var/cache/apt/archives/lock
	apt install -f vsftpd -yy 2>/dev/null
fi
sleep 1

echo -e "===>\tAyarlar /etc/vsftpd.conf dosyasına işleniyor..."
if [ ! "$(cat /etc/vsftpd.conf | tail -1)" == "allow_writeable_chroot=YES" ];then
	echo -e "utf8_filesystem=YES\nwrite_enable=YES\nchroot_local_user=YES\nallow_writeable_chroot=YES" >> /etc/vsftpd.conf
fi
sleep 1

echo -e "===>\tvsftpd.service yeniden başlatılıyor..."
systemctl restart vsftpd
systemctl daemon-reload
sleep 1

echo -e "===>\tTüm kullanıcılar için Tarama klasörü oluşturuluyor..."

for dizin in /home /home/DIB
do
	if [ -d ${dizin} ];then
		for user in $(ls ${dizin} | grep -v DIB | grep -v lost+found)
		do
			kullanici_home=$(eval echo "~${user}" 2>/dev/null)
			kullanici_name=$(echo $kullanici_home 2>/dev/null | sed 's|.*/||')
			kullanici_grup=$(id -gn $kullanici_name 2>/dev/null) 

			if [[ `echo $kullanici_home | grep -w "DIB"` == "DIB" ]];then
				mkdir -p "$kullanici_home/Tarama" 1>/dev/null 2>/dev/null
				chmod 777 -R "$kullanici_home/Tarama"
				chown $kullanici_name:"$kullanici_grup" "$kullanici_home/Tarama"

				sed -i 's/yes/no/' /usr/share/pam-configs/pam_script
				sed -i 's/yes/no/' /usr/share/pam-configs/krb5
				pam-auth-update --force --package
				pam-auth-update --force --remove pam_script
				pam-auth-update --force --remove krb5
				pam-auth-update --force --package
				systemctl restart vsftpd
				systemctl daemon-reload
				ln -sfr "$kullanici_home/Tarama" "$kullanici_home/Masaüstü/"
			else
				mkdir -p "$kullanici_home/Tarama" 1>/dev/null 2>/dev/null
				chmod 777 -R "$kullanici_home/Tarama" 2>/dev/null
				chown $kullanici_name:"$kullanici_grup" "$kullanici_home/Tarama" 2>/dev/null
				ln -sfr "$kullanici_home/Tarama" "$kullanici_home/Masaüstü/" 2>/dev/null
			fi
		done
	fi
done

echo -e "===>\tKurulum başarılı bir şekilde tamamlandı..."

IP=$(hostname -I)
echo
echo -e "===>\tŞimdi Tarayıcınızın IP adresini Google Chrome veya Firefox üzerinden girerek,"
echo -e "    \tAğ Tarayıcınızın Adres Defterinin FTP bölümüne aşağıdaki bilgilere göre kayıt ekleyin veya güncelleyin."
echo
echo -e "===>\tHostname\t: $IP"
echo -e "===>\tPort Number\t: 21"
echo -e "===>\tPath\t\t: Tarama"
echo -e "===>\tLogin User Name\t: Bilgisayar Giriş Kullanıcı Adınız"
echo -e "===>\tLogin Password\t: Bilgisayar Giriş Şifreniz"
