#!/usr/bin/env bash

if [[ "$(whoami)" != root ]]; then
	echo -e "===>\tLütfen yetkili hesap ile işlem yapınız."
	echo -e "===>\tÖrnek: sudo bash $0"
	exit 1
fi

kullanici_home=$(dirname $XAUTHORITY)
kullanici_name=$(echo $kullanici_home | sed 's|.*/||')

ls -d */ | tr " " "\n"

read -p "Lütfen paylaşacağınız klasörün adını giriniz: " PAYLASIM_ADI

echo "
[${PAYLASIM_ADI^^}]
   comment = Dosya paylaşım klasörü
   path = $kullanici_home/$PAYLASIM_ADI/
   browseable = yes
   guest ok = yes
   read only = no
   public = yes
   writable = yes
   create mask = 0777
" >> /etc/samba/smb.conf


