# AMAÇ?

* Bu scriptin amacı SMB Version 1 Protokolünün çalışmadığı ağ yazıcılarında bilgisayara tarama (Scan To PC) işlevini gerçekleştirmek için FTP protokolünü devreye almak için hazırlanmıştır.


# KURULUM:

* Kurulum için vsftpd.sh dosyasını indirdirkten sonra, `sudo bash vsftpd.sh` olarak çalıştırmanız yeterlidir.

# KURULUM VİDEOSU:
[<img src="https://gitlab.com/yahyayildirim/test/-/raw/main/video_and_picture/cinnamon_fare_sag_tik/poster-image.png">](https://gitlab.com/yahyayildirim/test/-/raw/main/video_and_picture/vsftpd/kyocera_vsftpd.mp4)
